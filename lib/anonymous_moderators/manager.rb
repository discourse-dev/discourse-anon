# frozen_string_literal: true

module DiscourseAnonymousModerators
  class Manager

    def self.get_parent(user)
      return unless SiteSetting.anonymous_moderators_enabled
      return unless user

      if anonymous_link = Link.find_by(user: user, deactivated_at: nil)
        fetch_parent_user(anonymous_link)
      else
        raise Discourse::NotFound
      end
    end

    def self.get_child(user)
      return unless SiteSetting.anonymous_moderators_enabled
      return unless user

      anonymous_link = Link.find_by(parent_user: user, deactivated_at: nil)
      if anonymous_link.nil?
        anonymous_link = create_child(user)
      end

      fetch_anonymous_user(anonymous_link)
    end

    def self.add_anon_user_to_groups(user, group)
      # do they have an anon account?
      return true if (!acceptable_parent?(user))
      
      # because they do, so let's update their anon groups.
      anon = Link.find_by(parent_user: user, deactivated_at: nil).user
      group.add(anon)

    end
    
    # if already anon account, remove the master, too.
    def self.remove_anon_from_groups(u, g)

      # check if group admin deleting an anon account already
      deleting_anon = true if Link.exists?(user: u) # (if acceptable child false, then already anon)

      # if we're already trying to delete anon, just find delete the parent too!
      if deleting_anon
        par = Link.find_by(user: u)&.parent_user
        if par
          g.remove(par)
        end
      else
        # if not deleting anon account already, then we're deleting a master account.
        # just delete the anon account associated with it, if it exists in the group.
        an = Link.find_by(parent_user: u)&.user
        if an
          g.remove(an)
        end
      end
    end
    

    def self.update_user(u)
      # check whether anon exists, and then if they do, port the changes from u to anon.
      Rails.logger.warn("UPDATE USER!")
      anon = Link.find_by(parent_user: u)&.user
      Rails.logger.warn("anon?")
      Rails.logger.warn(anon.username)

      if (!anon.nil?)
        Rails.logger.warn("not anon nil.")
        params = {
          name: u.username,
          moderator: u.moderator,
          trust_level: u.trust_level,
          groups: u.groups
        }

        # now merge the params with the user.
        params.merge!(username: anon.username)
      else
        # no anon account, so check whether already anon by accessing parent/master
        par = Link.find_by(user: u)&.parent_user
        Rails.logger.warn("parent: "+ par.username)
        if (!par.nil?)
          params = {
            name: u.username,
            moderator: u.moderator,
            trust_level: u.trust_level,
            groups: u.groups
          }

          params.merge!(username: par.username)
        else
          # user has no anon account!
          nil
        end
      end
      Rails.logger.warn("FINISH UPDATE USER!")
    end

    private

    def self.fetch_anonymous_user(anonymous_link)
      raise Discourse::InvalidAccess unless acceptable_link?(anonymous_link)
      anonymous_link.user.update!(Manager.enforced_child_params(parent: anonymous_link.parent_user, child: anonymous_link.user))
      anonymous_link.user
    end

    def self.fetch_parent_user(anonymous_link)
      raise Discourse::InvalidAccess unless acceptable_link?(anonymous_link)
      anonymous_link.parent_user
    end

    def self.acceptable_parent?(user)
      # return false if !user.staff?
      return false if Link.exists?(user: user) # Is already a child
      true
    end

    def self.acceptable_child?(user)
      return false if user.admin
      return false if Link.exists?(parent_user: user) # Is a parent
      true
    end

    def self.acceptable_link?(anonymous_link)
      return false unless acceptable_child?(anonymous_link.user)
      return false unless acceptable_parent?(anonymous_link.parent_user)
      true
    end

    def self.create_child(user)
      raise Discourse::InvalidAccess unless acceptable_parent?(user)
      User.transaction do
        create_params = {
          password: SecureRandom.hex,
          approved_at: 1.day.ago,
          created_at: 1.day.ago # bypass new user restrictions
        }

        create_params.merge!(enforced_child_params(parent: user))

        child = User.create!(create_params)

        child.user_option.update_columns(
          email_digests: false
        )

        Link.create!(user: child, parent_user: user, last_used_at: Time.zone.now)
      end
    end

    def self.enforced_child_params(parent: , child: nil)
      username = child&.username || UserNameSuggester.suggest(SiteSetting.anonymous_moderators_username_prefix)
      email = parent.email.sub('@', "+#{username}@") # Use plus addressing

      params = {
        email: email,
        skip_email_validation: true,
        name: username, # prevents error when names are required
        active: true,
        trust_level: 1,
        manual_locked_trust_level: 1,
        moderator: parent.moderator,
        groups: parent.groups,
        trust_level: parent.trust_level,
        approved: true
      }
      params.merge!(username: username) unless child

      params
    end

  end
end
