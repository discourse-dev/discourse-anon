# frozen_string_literal: true

module ::DiscourseAnonymousModerators
  class SwitchController < ::ApplicationController
    requires_plugin PLUGIN_NAME
    before_action :ensure_logged_in

    def become_child
      user = Manager.get_child(current_user)
      
      draft_sequence = DraftSequence.current(current_user, Draft::NEW_TOPIC)
      Rails.logger.warn("above")
      Rails.logger.warn(current_user)
      Rails.logger.warn(draft_sequence)
      draft = Draft.get(current_user, Draft::NEW_TOPIC, draft_sequence) if current_user
      Rails.logger.warn(draft.to_s)
      Rails.logger.warn("below121")
      
      
      if user
        # check for draft posts...
        dr = Draft.find_by(user_id: current_user, draft_key: "new_topic")
        if !dr.nil?
          ds = DraftSequence.find_by(user_id: current_user, draft_key: dr.draft_key)

          # does anon have a draft topic already? replace if so.
          an_dr = Draft.find_by(user_id: user.id, draft_key: "new_topic")
          if (!an_dr.nil?)
            nil
            # Draft.clear(an_dr.id, an_dr.draft_key, an_dr.sequence)
          end

          # transfer ownership of the draft and draft sequence to the
          # anon account.
          new_an_ds = DraftSequence.next!(user.id, dr.draft_key)
          dr.update(user_id: user.id, sequence: new_an_ds)

          draft_sequence = DraftSequence.current(user, Draft::NEW_TOPIC)
          Rails.logger.warn("above")
          Rails.logger.warn(user)
          Rails.logger.warn(draft_sequence)
          draft = Draft.get(user, Draft::NEW_TOPIC, draft_sequence) if user
          Rails.logger.warn(draft.to_s)
          Rails.logger.warn("below121")
          
        end


        # if (!dr.nil?)
        #   # now we check for any drafts the anon user has, and flush
        #   # them!
        #   if (!an_dr.nil?)
        #     # anon draft to be deleted.
        #     Draft.clear(an_dr.id, an_dr.draft_key, an_dr.sequence)
        #   end

        #   #save draft to anon
        #   dr.update(user_id: user.id)
        #   if (!ds.nil?)
        #     ds.update(user_id: user.id)
        #   else
        #     ds.something?
        #   end
        # end
        log_on_user(user)
        render json: success_json
      else
        failed_json
      end
    end

    def become_parent
      user = Manager.get_parent(current_user)

      if user
        # have you written a draft as anon?
        dr = Draft.find_by(user_id: current_user, draft_key: "new_topic")
        if (!dr.nil?)
          ds = DraftSequence.find_by(user_id: current_user, draft_key: dr.draft_key)

          # does master have a draft topic already? replace if so.
          ms_dr = Draft.find_by(user_id: user.id, draft_key: "new_topic")
          if (!ms_dr.nil?)
            nil
            # Draft.clear(an_dr.id, an_dr.draft_key, an_dr.sequence)
          end

          # transfer ownership of the draft and draft sequence to the
          # anon account.
          new_ms_ds = DraftSequence.next!(user.id, dr.draft_key)
          dr.update(user_id: user.id, sequence: new_ms_ds)
        end

        log_on_user(user)
        render json: success_json
      else
        failed_json
      end
    end
  end
end
