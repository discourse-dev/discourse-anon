# frozen_string_literal: true

# name: discourse-anonymous-moderators
# about: Allow moderators to have an alternative account for performing actions
# version: 1.0
# authors: David Taylor
# url: https://github.com/discourse/discourse-anonymous-moderators

enabled_site_setting :anonymous_moderators_enabled

require_relative "lib/anonymous_moderators/engine"
require_relative "lib/anonymous_moderators/manager"

register_asset 'stylesheets/anonymous_moderators.scss'

after_initialize do

  add_to_class(:user, :is_anonymous_moderator) do
    return DiscourseAnonymousModerators::Link.exists?(user: self)
  end

  add_to_class(:user, :can_become_anonymous_moderator) do
    return DiscourseAnonymousModerators::Manager.acceptable_parent?(self)
  end

  add_to_class(:user, :get_child) do
    child = DiscourseAnonymousModerators::Link.find_by(parent_user: self, deactivated_at: nil)
    return User.find_by(id: child)
  end

  add_to_class(:user, :get_parent) do
    parent = DiscourseAnonymousModerators::Link.find_by(user: self, deactivated_at: nil)
    return User.find_by(id: parent)
  end

  # add notifications for parent so they see notifications for what anon has done
  add_to_class(:topic_user, :add_parent_notifications) do
    return DiscourseAnonymousModerators::Manager.add_parent_notifications(:current_user, topic: self)
  end  

  add_to_serializer(:current_user, :is_anonymous_moderator) do
    object.is_anonymous_moderator
  end

  add_to_serializer(:current_user, :can_become_anonymous_moderator) do
    object.can_become_anonymous_moderator
  end

  add_to_serializer(:current_user, :get_child) do
    object.get_child
  end

  add_to_serializer(:current_user, :get_parent) do
    object.get_parent
  end

  add_model_callback("DiscourseAnonymousModerators::Link", :after_commit, on: [ :create, :update ]) do
    UserCustomField.find_or_initialize_by(user: user, name: :parent_user_username).update_attributes!(value: parent_user.username)
  end

  # TODO: manage this happening. a user has been updated, so check if they have had attributes like 
  # mod status updated etc. and update on the other account.
  # add_model_callback(User, :after_commit, on: :update) do |user_to_change|
  #   # Rails.logger.warn("current user " +current_user.username)
  #   Rails.logger.warn("user_to_change " +user_to_change.username)

  #   # DiscourseAnonymousModerators::Manager.update_user(self)
  # end

  whitelist_public_user_custom_field :parent_user_username

  DiscourseEvent.on(:topic_created) do |topic|
    return true if DiscourseAnonymousModerators::Link.exists?(self)
    
    parent = DiscourseAnonymousModerators::Link.find_by(user_id: topic.user_id)&.parent_user
    if (parent != nil)
      # now create the new notification for the parent user.
      TopicUser.auto_notification_for_staging(
        parent.id, 
        topic.id, 
        TopicUser.notification_reasons[:created_post]
      )
    end
  end

  DiscourseEvent.on(:user_added_to_group) do |user, group|
    DiscourseAnonymousModerators::Manager.add_anon_user_to_groups(user, group)
  end

  DiscourseEvent.on(:user_removed_from_group) do |user, group|
    DiscourseAnonymousModerators::Manager.remove_anon_from_groups(user, group)
  end

  # TODO: handle this - adjust trust for the other user.
  DiscourseEvent.on(:user_promoted) do |user|
    Rails.logger.warn("user promoted!!")
  end

  module ModifyUserEmail
    def execute(args)
      return super(args) unless SiteSetting.anonymous_moderators_enabled

      if parent = DiscourseAnonymousModerators::Link.find_by(user_id: args[:user_id])&.parent_user
        args[:to_address] = parent.email
      end
      super(args)
    end
  end

  ::Jobs::UserEmail.class_eval do
    prepend ModifyUserEmail
  end
end
