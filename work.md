# Docs
### Written by Ben Cartwright, 2019

## Intro
I've done some work to make Discourse work better for the research school of computer science at ANU. I've extended the work done by [David Taylor](https://github.com/discourse/discourse-anonymous-moderators), which formed a good basis for the extensions and improvements I offer below.

## Improvements

1. Anon posting enabled for all valid accounts (those which are approved and can log on.)
2. Anon account easily known to any member of staff (tutors and lecturers)
3. Switching into anon account more accessible than vanilla Discourse
4. If switching to anon and in the midst of a post, the draft persists to the new account
5. When the anon account is created, the groups and traits of the user persist to the new account
6. When the anon account is created after the group, adding a master user to anther group will automatically add their anon account
7. When anon person removed from a group, so is their master account
8. Anon notifications for posts propagate up to the master account

## Things to Add Still
1. Drafts don't persist if they aren't topic posts
2. Switching to anon in the composer with a more user friendly method. Reloading isn't the issue (drafts ensure no lost data there) but style is.
3. Changing characteristics about a parent or anon user isn't reflected in the other account after initial account creation!


## Installation
There are two different ways to install Discourse. One uses Docker, the other is a local
dev environment using Ruby (which Docker also uses, but it's behind the abstraction of a
container!)

This plugin should work with both (untested with the Docker install, but in theory there is no fundamental difference), yet it was built with a local install on MacOS.

The install process is simple for Docker.
1. fetch the relavent Git https link;
2. go to your Docker installation's yaml config file;
3. Find where it allows you to install a plugin (under hooks > after_code > exec > cmd > new bullet point);
4. Now, simply enter the `git clone link` as a new entry there and rebuild your container!


For local developers:
1. In your installation environment, ensure everything is up to date (`bundle update`);
2. Simply `git clone` the same Git https link inside `{path to discourse}/plugins/`;
3. Stop your server;
4. Run `bundle exec rake db:migrate`;
5. Start up your server again (`bundle exec rails server`)

Now, for both types of install, the only thing left to do is to go into `localhost:port/admin/plugins` and enable discourse-anon-moderators.

That's **all** done. You should be able to utilise this functionality now.


## How does all this work?

Okay, so here are some basic details.
1. The plugin.rb file is the basic building block upon which all plugins rest. It's important because it defines what behaviour is exposed to the rest of discourse, what your frontend will see, etc. etc.
2. in the library (`lib/`) you'll see an engine (admin to connect us to the discourse rails engine... this isn't very important) and a manager (this is important; it houses a bunch of different functions you'll see referenced in the plugin.rb file.)
3. in config you'll see routes.rb, which is important because any links you want need to have behaviour attached to it, and this is where that is mapped. You'll also see locales (files which contain any text you want the frontend to show) and a subdir `db/` which contains the files used to initially modify the database so we can store info. I didn't use this (I was able to use the linking system made), but it may interest you. 
4. `assets/` which contains any files we want sent to the user, or anything frontend! in `javascript/`, you can see components, initialisers and connectors. components are functions computed which give information from our model to our frontend; connectors define what is to be sent between the model and the frontend `div`'s; finally, the 'initializer' is useful because whenever we load the page, this runs first and it contains all the instructions and logic about how we want to modify the frontend for our plugin. We interact with our backend through the 'plugin-api'.
5. the switch controller knows what to do when we're moving between anon and master. whenever this happens, we might need to check things or execute code (which I infact do need to do for this system) so this is where you can do that.
6. `models/link.rb` is a bit of an enigma, but it seems to represent the database relation between the master and anon account in Ruby. I only use it in this project to find out an anon account from a master, and a master from an anon.